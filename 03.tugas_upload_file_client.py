# import library socket karena menggunakan IPC socket
import socket

# definisikan IP server tujuan file akan diupload
SERVER_IP = '192.168.150.1'

# definisikan port number proses di server
SERVER_PORT = 1612

# definisikan ukuran buffer untuk mengirim
BUFFER_SIZE = 2048

# buat socket (apakah bertipe UDP atau TCP?)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan koneksi ke server
sock.connect((SERVER_IP, SERVER_PORT))

# buka file bernama "file_diupload.txt bertipe byte
file = open('file_diupload.txt',  'rb')

# masih hard code, file harus ada dalam folder yang sama dengan script python
file = open('file_diupload', 'rb')

try:
    # baca file tersebut sebesar buffer
    buffer = file.read(BUFFER_SIZE)

    # selama tidak END OF FILE; pada pyhton EOF adalah b''
    while buffer != b'':
        # kirim hasil pembacaan file
        sock.send(buffer)

        # baca sisa file hingga EOF
        buffer = file.read(BUFFER_SIZE)

        #print(byte)
finally:
    print ("end sending")

    # tutup file jika semua file telah  dibaca
    file.close()

# tutup koneksi setelah file terkirim
sock.close()
