# import library socket karena menggunakan IPC socket
import socket

# definisikan IP untuk binding
SERVER_IP ='192.168.150.1'

# definisikan port untuk binding
SERVER_PORT = 1612

# definisikan ukuran buffer untuk menerima pesan
BUFFER_SIZE = 2048

# buat socket (bertipe UDP atau TCP?)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan binding ke IP dan port
sock.bind((SERVER_IP, SERVER_PORT))

# lakukan listen
sock.listen()

#  siap menerima koneksi
conn, addr = sock.accept()
print ('Connection address:', addr)

# buka/buat file bernama hasil_upload.txt untuk menyimpan hasil dari file yang dikirim server
file = open('hasil_upload.txt',  'wb')

# masih hardcoded nama file, bertipe byte
file = open('hasil_upload.txt',  'wb')


# loop forever
while 1:
    # terima pesan dari client
    buffer = conn.recv(BUFFER_SIZE)

    # tulis pesan yang diterima dari client ke file kita (result.txt)
    file.write(buffer)

    # berhenti jika sudah tidak ada pesan yang dikirim
    if not buffer: break


# tutup file result.txt
file.close()

#tutup socket
conn.close()

# tutup koneksi
sock.close()
