# import library socket karena akan menggunakan IPC socket
import socket

## definisikan alamat IP bind dari server
UDP_ IP = "192.168.150.1"

# definisikan port number untuk bind dari server
UDP_PORT = 7777

# buat socket bertipe UDP
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# lakukan bind
sock.bind((UDP_IP, UDP_PORT))

# loop forever
while True:
    # terima pesan dari client
    data, addr = sock.recvfrom(1024)
    # menampilkan hasil pesan dari client
    print ("Pesan diterima:", data.decode())
    #print (addr)
    print (addr)
