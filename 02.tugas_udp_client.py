# import library socket karena akan menggunakan IPC socket
import socket

# definisikan target IP server yang akan dituju
UDP_IP = "192.168.150.1"

# definisikan target port number server yang akan dituju
UDP_PORT = 7777
PESAN = "BALA BALA"

#print ("target IP:", UDP_IP)
print ("target IP:", UDP_IP)

#print ("target port:", UDP_PORT)
print ("target port:", UDP_PORT)

#print ("pesan:", PESAN)
print ("pesan:", PESAN)

# buat socket bertipe UDP
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# lakukan loop 10 kali
for x in range (10):
    # definisikan pesan yang akan dikirim
    MSG = '%s ke-%' % (PESAN, x+1)

    # kirim pesan
    sock.sendto(str.encode(MSG), (UDP_IP, UDP_PORT))
