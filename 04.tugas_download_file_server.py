# import library socket karena menggunakan IPC socket
import socket

# definisikan IP untuk binding
SERVER_IP ='192.168.150.1'

# definisikan port untuk binding
SERVER_PORT = 7777

# definisikan ukuran buffer untuk menerima pesan
BUFFER_SIZE = 1024

# buat socket (bertipe UDP atau TCP?)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan binding ke IP dan port
sock.bind((SERVER_IP, SERVER_PORT))

# lakukan listen
sock.listen()

#  siap menerima koneksi
conn, addr = s.accept()
print(addr)

# buka file bernama "file_didownload.txt
file = open('file_didownload.txt', 'rb')

# masih hard code, file harus ada dalam folder yang sama dengan script python
file = open('file_didownload.txt', 'rb')

try:
    # baca file tersebut sebesar buffer
    buffer = file.read(BUFFER_SIZE)

    # selama tidak END OF FILE; pada pyhton EOF adalah b''
    while buffer != b'':
        # kirim hasil pembacaan file dari server ke client
        conn.send(buffer)

        # baca sisa file hingga EOF
        buffer = file.read(BUFFER_SIZE)

finally:
    print ("end sending")

    # tutup file jika semua file telah  dibaca
    file.close()

# tutup socket
conn.close()

# tutup koneksi
sock.close()
