# import library socket karena menggunakan IPC socket
import socket

# definisikan IP server tujuan file akan diupload
SERVER_IP = '192.168.150.1'

# definisikan port number proses di server
SERVER_PORT = 7777

# definisikan ukuran buffer untuk mengirim
BUFFER_SIZE = 1024

# buat socket (apakah bertipe UDP atau TCP?)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


# lakukan koneksi ke server
sock.connect((SERVER_IP, SERVER_PORT))

# buka file bernama "hasil_download.txt bertipe byte
file = open('hasil_download.txt','wb')

# masih hard code, file harus ada dalam folder yang sama dengan script python
file = open('hasil_download.txt','wb')

# loop forever
while 1:
     # terima pesan dari client
    buffer = sock.recv(BUFFER_SIZE)

    # tulis pesan yang diterima dari client ke file kita (result.txt)
    file.write(buffer)

    # berhenti jika sudah tidak ada pesan yang dikirim
    if not buffer: break

# tutup file_hasil_download.txt
file.close()

#tutup socket
sock.close()
print('file succesfully downloaded')
